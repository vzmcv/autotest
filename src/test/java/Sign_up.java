import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Random;

public class Sign_up {

    @Test
    public void signUp () {

        System.setProperty("webdriver.chrome.driver", "/Users/User/chromeDriver/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.get("https://portal.dev.influential.co/");

        
        WebElement header = driver.findElement(By.id("header"));
        header.findElement(By.xpath("//*[text()='Get Started']")).click();

        WebDriverWait wait = new WebDriverWait(driver, 10);

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("main-signup")));

        driver.findElement(By.xpath("//*[@href='/signup/talent#form-anchor']")).click();

        driver.findElement(By.id("name-first")).sendKeys("Vlad");

        driver.findElement(By.id("name-last")).sendKeys("Zemtcov");

        Random random = new Random();

        int n = random.nextInt(100) + 1;

        String email = "Zemec" + n + "@bk.ru";
        String username = "VladZemtcov" + n;

                System.out.println(email);
                System.out.println(username);

        driver.findElement(By.id("email")).sendKeys(email);

        driver.findElement(By.id("company-name")).sendKeys("ZEMEC" );

        driver.findElement(By.id("username")).sendKeys(username);

        driver.findElement(By.id("password")).sendKeys("123123qwe");

        driver.findElement(By.id("password-confirm")).sendKeys("123123qwe");

        driver.findElement(By.cssSelector("button[type=\"submit\"]")).click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("signup-complete")));



       driver.quit();
    }

    @Test
    public void signUpfail () {

        System.setProperty("webdriver.chrome.driver", "/Users/User/chromeDriver/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.get("https://portal.dev.influential.co/");

        driver.findElement(By.xpath("//*[text()='Get Started']")).click();

        WebDriverWait wait = new WebDriverWait(driver, 10);

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("main-signup")));

        driver.findElement(By.xpath("//*[@href='/signup/talent#form-anchor']")).click();

        driver.findElement(By.xpath("//*[@type='submit']")).click();

        WebElement nameFirstMessage = driver.findElement(By.xpath("//div[input[@id='name-first']]/p[contains(@class,'error-msg')]"));
        Assert.assertEquals(nameFirstMessage.getText(),"PLEASE ADD A FIRST NAME");

        WebElement nameLastMessage = driver.findElement(By.xpath("//div[input[@id='name-last']]/p[contains(@class,'error-msg')]"));
        Assert.assertEquals(nameLastMessage.getText(),"PLEASE ADD A LAST NAME");

        WebElement emailMessage = driver.findElement(By.xpath("//div[input[@id='email']]/p[contains(@class,'error-msg')]"));
        Assert.assertEquals(emailMessage.getText(),"PLEASE ADD AN EMAIL ADDRESS");

        WebElement companyNameMessage = driver.findElement(By.xpath("//div[input[@id='company-name']]/p[contains(@class,'error-msg')]"));
        Assert.assertEquals(companyNameMessage.getText(), "PLEASE ADD A COMPANY NAME");

        WebElement usernameMessage = driver.findElement (By.xpath("//div[input[@id='username']]/p[contains(@class,'error-msg')]"));
        Assert.assertEquals(usernameMessage.getText(),"PLEASE ADD A USERNAME");

        WebElement passwordMessage = driver.findElement(By.xpath("//div[input[@id='password']]/p[contains(@class,'error-msg')]"));
        Assert.assertEquals(passwordMessage.getText(), "PLEASE ADD A PASSWORD");

        WebElement passwordConfirmMessage = driver.findElement(By.xpath("//div[input[@id='password-confirm']]/p[contains(@class,'error-msg')]"));
        Assert.assertEquals(passwordConfirmMessage.getText(), "PLEASE CONFIRM YOUR PASSWORD"); //Confirm Password"
        driver.quit();
    }

}